#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t box-panic-ubuntu --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
docker create --name box-panic-ubuntu box-panic-ubuntu
docker cp box-panic-ubuntu:/game/built built
docker rm box-panic-ubuntu
cd built
./box-panic
