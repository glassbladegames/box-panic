#include <slice.h>
#include <sliceopts.h>
#include <slicefps.h>

slList Rects = slList("rects",slNoIndex); // list of slBox*
slBU level = 0;
void NextLevel ();
struct Fragment
{
	slBox* box;
	slScalar age;
    slVec2 vel;
};
slList Fragments = slList("fragments",slNoIndex); // list of Fragment*
void SpawnFragment (slScalar x0, slScalar y0, slScalar x1, slScalar y1, slScalar xvel, slScalar yvel)
{
	slBox* box = slCreateBox();
	box->backcolor = {95,95,95,95};
    box->SetDims(slVec2(x0, y0), slVec2(x1 - x0, y1 - y0), 130);
	Fragment* fragment = malloc(sizeof(Fragment));
	fragment->box = box;
	fragment->age = 0;
	fragment->vel = slVec2(xvel,yvel) * .375;
	Fragments.Add(fragment);
};
void CreateFragments (slBox* box)
{
    slVec2 mouse = slGetMouse();
    slVec2 farxy = box->xy + box->wh;
    slVec2 ratio = (mouse - box->xy) / box->wh;
	SpawnFragment(box->xy.x, box->xy.y, mouse.x, mouse.y, -ratio.x,  -ratio.y );
	SpawnFragment(box->xy.x, mouse.y,   mouse.x, farxy.y, -ratio.x,  1-ratio.y);
	SpawnFragment(mouse.x,   box->xy.y, farxy.x, mouse.y, 1-ratio.x, -ratio.y );
	SpawnFragment(mouse.x,   mouse.y,   farxy.x, farxy.y, 1-ratio.x, 1-ratio.y);
};
slScalar LevelAge = 1000;
bool LevelWasReset;
void RectClick (slBox* box)
{
	CreateFragments(box);
	Rects.Remove(box);
	slDestroyBox(box);
	if (!Rects.itemcount)
	{
		LevelWasReset = false;
		NextLevel();
	};
};
slBox* levelbox;
slScalar TimeRemaining;
slBU best = 0;
void NextLevel ()
{
	level++;
	for (slBU cur = 0; cur < level; cur++)
	{
		slBox* box = slCreateBox();
		box->backcolor = {95,95,95,95};
		box->bordercolor = {191,191,191,191};
		box->hoverable = true;
		box->hoverbackcolor = {127,127,127,127};
		box->hoverbordercolor = {255,255,255,255};
		box->onclick = RectClick;
        box->SetDims(
            slVec2(
                ((rand() % 1001) / (slScalar)1000) * 0.9,
                ((rand() % 1001) / (slScalar)1000) * 0.9
            ),
            slVec2(0.1),
            120
        );
        Rects.Add(box);
	};
	if (level > best) best = level;
	char* leveltext;
	asprintf(&leveltext,"Level %u (Best: %u)",(unsigned int)level,(unsigned int)best);
	levelbox->SetTexRef(slRenderText(leveltext));
	free(leveltext);
	TimeRemaining = 5 + (level - 1) / 5;
	LevelAge = 0;
};
void StepFragments ()
{
	slScalar delta = slGetDelta();
	for (slBU cur = 0; cur < Fragments.itemcount; cur++)
	{
		Fragment* frag = *(Fragments.items + cur);
		frag->age += delta;
		if (frag->age > 1)
		{
            Fragments.Remove(frag);
			slDestroyBox(frag->box);
			free(frag);
			cur--;
			continue;
		};
        frag->box->xy += frag->vel * delta;
		frag->box->backcolor.a = (1 - frag->age) * 95;
	};
};
int main ()
{
	slInit("Box Panic");
	fpsInit();
	slGetKeyBind("Skip Logos",slKeyCode(SDLK_ESCAPE))->onpress = OnSkipLogo;
	DefaultLogoSequence(false,false);
	opInit();

	levelbox = slCreateBox();
    levelbox->SetDims(slVec2(0,0.0125),slVec2(1,0.05),100);
	slBox* timebox = slCreateBox();
    timebox->SetDims(slVec2(0,0.9375),slVec2(1,0.05),100);
	FILE* file = fopen("topscore.txt","r");
	if (file)
	{
		if (fscanf(file,"%u",&best) != 1) best = 0;
		fclose(file);
	};

	NextLevel();
	while (!slGetExitReq())
	{
        TimeRemaining -= slGetDelta();
		char* timetext;
		asprintf(&timetext,"%u ms",(unsigned int)((TimeRemaining * 1000) + 0.5));
        timebox->SetTexRef(slRenderText(timetext));
		free(timetext);
		if (TimeRemaining < 0)
		{
			while (Rects.itemcount)
			{
				slBox* rect = *(Rects.items);
                Rects.Remove(rect);
				slDestroyBox(rect);
			};
			level = 0;
			LevelWasReset = true;
			NextLevel();
		};
		StepFragments();
		LevelAge += slGetDelta();
		if (LevelAge < 0.5)
		{
			Uint8 changing = (LevelAge * 2) * 255;
			if (LevelWasReset)
			{
				levelbox->drawmask.r = 255;
				levelbox->drawmask.g = changing;
			}
			else
			{
				levelbox->drawmask.r = changing;
				levelbox->drawmask.g = 255;
			};
			levelbox->drawmask.b = changing;
		}
		else levelbox->drawmask = {255,255,255,255};
		slCycle();
	};

	file = fopen("topscore.txt","w");
	if (file)
	{
		fprintf(file,"%u",best);
		fclose(file);
	};
	// Don't have to manually clean these up (slQuit will do it if not) but it's good practice.
	slDestroyBox(levelbox);
	slDestroyBox(timebox);

	opQuit();
	fpsQuit();
	slQuit();
};
