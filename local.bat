rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t box-panic-windows --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name box-panic-windows box-panic-windows
docker cp box-panic-windows:/game/built built
docker rm box-panic-windows
cd built
box-panic.exe
